public class Data {
    private String name;
    private Group[] groups;

    public Data(){}
    public Data(String name, Group ... groups){
        this.name = name;
        this.groups = groups;
    }

    public String getName() { return name; }
    public Group[] getGroups() { return groups; }

    public void setName(String name) { this.name = name; }
    public void setGroups(Group ... groups) { this.groups = groups; }

    public  int getLength(){ return groups.length; }
    public int getCount(){
        int count = 0;
        for(Group group : groups){
            for(int elem : group.getArray()){
                count++;
            }
        }
        return count;
    }

    public IteratorData iterator() { return new IteratorData(this); }
}
