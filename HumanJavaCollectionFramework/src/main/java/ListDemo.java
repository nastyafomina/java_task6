import java.util.*;

public class ListDemo {
    public List getListOfHomonyms(List<Human> humans, Human person) {
        List<Human> homonym = new ArrayList<>();
        for (int i = 0; i < humans.size(); i++) {
            if (person.getSurname().equals(humans.get(i).getSurname())){
                homonym.add(humans.get(i));
            }
        }
        return homonym;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! См.тест
    */
    public List getTheOldestHumans(List<Human> humans){
        List<Human> oldHumans = new ArrayList<>();
        if(!humans.isEmpty()) {
            int maxAge = humans.get(0).getAge();
            for (int i = 0; i < humans.size(); i++) {
                if (humans.get(i).getAge() > maxAge) {
                    maxAge = humans.get(i).getAge();
                }
            }
            for (int i = 0; i < humans.size(); i++) {
                if (humans.get(i).getAge() == maxAge) {
                    oldHumans.add(humans.get(i));
                }
            }
        }
        return oldHumans;
    }
}
