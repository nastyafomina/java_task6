import java.util.*;

public class DataDemo {
    public static List getAll(Data data) {
        IteratorData iterator = data.iterator();
        List<Integer> list = new ArrayList<>();
        while(iterator.hasNext()){
            list.add(iterator.next());
        }
        return list;
    }
}
