import java.util.*;

public class CollectionsDemo {
    public int checkFistElement(List<String> string, char symbol){
        int count = 0;
        for(int i=0;i<string.size();i++){
            if(string.get(i).indexOf(symbol) == 0){
                count++;
            }
        }
        return count;
    }

    /* Филиппов А.В. 23.06.2020 Комментарий не удалять.
      Не работает! Читаем задание.
      При изменении элементов входного списка элементы выходного изменяться не должны.
    */
    public List copyListWithoutPerson(List<Human> humans, Human person) {
        List<Human> humansCopy = new ArrayList<>();
        for(Human human : humans){
            if(!human.equals(person)) {
                humansCopy.add(new Human(human.getSurname(), human.getName(), human.getPatronymic(), human.getAge()));
            }
        }
        return humansCopy;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! См.тест. Плюс не нужно портить входной список и нужно параметризовать выходной (это я поправил).
    */
    public List<Set<Integer>> disjointSets(List<Set<Integer>> listSet, Set<Integer> set){
        List<Set<Integer>> listSetReturn = new ArrayList<>();
        for(Set setInt : listSet) {
            for (int elem : set) {
                if (setInt.contains(elem)) {
                    listSetReturn.add(setInt);
                }
            }
        }
        return listSetReturn;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! Читаем описание метода toArray()
     If this set makes any guarantees as to what order its elements are returned by its iterator,
     this method must return the elements in the same order.
     Готов написать сет без такой гарантии, после чего ваша процедура перестанет работать.
     Либо сократите количество вызовов toArray() до одного, либо используйте что-то типа
     for(Integer number : setNumbers) {
     }
    */
    public Set getSetAccordingToIdentifiers(Map<Integer, Human> map, Set<Integer> setNumbers){
        Set<Human> humans = new HashSet<>();
        for(Integer number : setNumbers){
            if(map.containsKey(number)){
                humans.add(map.get(number));
            }
        }
        return humans;
    }

    public List adultsIdList(Map<Integer, Human> map){
        List<Integer> adultsId = new ArrayList<>();
        for(Map.Entry<Integer, Human> current : map.entrySet()) {
            if (current.getValue().getAge() >= 18){
                adultsId.add(current.getKey());
            }
        }
        return adultsId;
    }

    public Map mapIdAge(Map<Integer, Human> map){
        Map<Integer,Integer> mapIdAge = new HashMap<>();
        for(Map.Entry<Integer, Human> current : map.entrySet()) {
            mapIdAge.put(current.getKey(), current.getValue().getAge());
        }
        return mapIdAge;
    }

    public Map coevalsMap(Set<Human> humanSet){
        Map<Integer, List<Human>> coevals = new HashMap<>();
        /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
         Зачем здесь объявлять переменные? Двумя строчками при первом присвоении им самое место.
        */
        for(Human human: humanSet){
            List<Human> humanList = new ArrayList<>();
            int age = human.getAge();
            for(Human current: humanSet) {
                if (current.getAge() == age) {
                    humanList.add(current);
                }
            }
            coevals.put(age, humanList);
        }
        return coevals;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает!
     1. с использованием метода coevalsMap
     2. списки должны быть отсортированы по фио
    */
    public Map mapAgeAlphabetSurname(Set<Human> humanSet){
        /*List<Human> humanList;
        Map<Character, List<Human>> humanChar;
        Map<Integer, Map<Character, List<Human>>> map = new HashMap<>();
        int age;
        char letter;
        for(Human human : humanSet) {
            humanChar = new HashMap<>();
            age = human.getAge();
            for(Human humanLetter : humanSet) {
                letter = humanLetter.getSurname().charAt(0);
                humanList = new ArrayList<>();
                for (Human current : humanSet) {
                    if (current.getSurname().charAt(0) == letter && current.getAge() == age) {
                        humanList.add(current);
                    }
                    Collections.sort(humanList, new Comparator<Human>() {
                        @Override
                        public int compare(Human o1, Human o2) {
                            if(o1.getSurname().compareTo(o2.getSurname()) == 0 && o1.getName().compareTo(o2.getName()) == 0) {
                                return o1.getName().compareTo(o2.getName()); }
                            if(o1.getSurname().compareTo(o2.getName()) == 0) {
                                return o1.getName().compareTo(o2.getName());
                            }
                            return o1.getSurname().compareTo(o2.getSurname());
                        }
                    });
                }
                if(!humanList.isEmpty()) {
                    humanChar.put(letter, humanList);
                }
            }
            map.put(age, humanChar);
        }
        return map;*/

        List<Human> humanList;
        Map<Character, List<Human>> humanChar;
        Map<Integer, Map<Character, List<Human>>> map = new HashMap<>();

        Map<Integer, Human> mapAge = coevalsMap(humanSet);

        for(Integer age : mapAge.keySet()){
            humanChar = new HashMap<>();
            for(Human humanLetter : humanSet) {
                char letter = humanLetter.getSurname().charAt(0);
                humanList = new ArrayList<>();
                for (Human current : humanSet) {
                    if (current.getSurname().charAt(0) == letter && current.getAge() == age) {
                        humanList.add(current);
                    }
                    Collections.sort(humanList, new Comparator<Human>() {
                        @Override
                        public int compare(Human o1, Human o2) {
                            if(o1.getSurname().compareTo(o2.getSurname()) == 0 && o1.getName().compareTo(o2.getName()) == 0) {
                                return o1.getName().compareTo(o2.getName()); }
                            if(o1.getSurname().compareTo(o2.getName()) == 0) {
                                return o1.getName().compareTo(o2.getName());
                            }
                            return o1.getSurname().compareTo(o2.getSurname());
                        }
                    });
                }
                if(!humanList.isEmpty()) {
                    humanChar.put(letter, humanList);
                }
            }
            map.put(age, humanChar);
        }
        return map;
    }
}

