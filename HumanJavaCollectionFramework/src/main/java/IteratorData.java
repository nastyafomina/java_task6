import java.util.*;

public class IteratorData implements Iterator {
    private Data data;
    private int index;

    public IteratorData(Data data){
        this.data = data;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        return index < data.getCount();
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! Читаем описание метода next()

    NoSuchElementException – if the iteration has no more elements
    */
    @Override
    public Integer next() {
        int count = 0;
        for (Group group : data.getGroups()) {
            for (int elem : group.getArray()) {
                if (count == index) {
                    index++;
                    return elem;
                }
                count++;
            }
        }
        throw new NoSuchElementException();
    }
}
