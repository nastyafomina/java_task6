public class Group {
    private int id;
    private int[] array;

    public Group(){}
    public Group(int id, int ... array){
        this.id = id;
        this.array = array;
    }

    public int getId(){ return id; }
    public int[] getArray(){ return array; }

    public void setId(int id){ this.id = id; }
    public void setArray(int ... array){ this.array = array; }

    public  int getLength(){ return array.length; }
}
