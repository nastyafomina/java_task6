import java.util.*;

public class PhoneBook {
    private Map<Human, List<String>> phoneBook;

    public PhoneBook(){ phoneBook = new HashMap<>(); }
    public PhoneBook(Human human, String phone){
        List<String> phones = new ArrayList<>();
        phones.add(phone);
        phoneBook = new HashMap<>();
        phoneBook.put(human, phones);
    }
    public PhoneBook(Human human, List<String> phones){
        phoneBook = new HashMap<>();
        phoneBook.put(human, phones);
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Стоит-ли делать класс, если есть метод, который дает доступ к внутреннему состоянию?
    */
   // public Map getPhoneBook(){ return phoneBook; }

    public void addHuman(Human human, List<String> phones){ phoneBook.put(human, phones); }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! см. тест
    */
    public void addPhone(Human human, String newPhone){
        if(!phoneBook.containsKey(human)){
            List<String> phoneList = new ArrayList<>();
            phoneList.add(newPhone);
            addHuman(human, phoneList);
        } else {
            List<String> phones = phoneBook.remove(human);
            phones.add(newPhone);
            phoneBook.put(human, phones);
        }
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! см. тест
    */
    public void deletePhone(Human human, String phone){
        if(phoneBook.containsKey(human)) {
            List<String> phones = phoneBook.remove(human);
            phones.remove(phone);
            phoneBook.put(human, phones);
        }
    }

    public Human getHumanByPhone(String phone){
        Set<Human> keys = phoneBook.keySet();
        for(Human key : keys) {
            if (phoneBook.get(key).contains(phone)){
                return key;
            }
        }
        return null;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! Должна возвращаться PhoneBook
    */
    public PhoneBook getHumansWithPhonesByBeginningOfSurname(String beginningOfSurname){
        PhoneBook phoneBookNew = new PhoneBook();
        Set<Human> keys = phoneBook.keySet();
        for(Human key : keys) {
            if (key.getSurname().startsWith(beginningOfSurname)){
                phoneBookNew.addHuman(key, phoneBook.get(key));
            }
        }
        return phoneBookNew;
    }

    /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
     Не работает! А где метод получения списка телефонов по человеку?
    */
    public List<String> getPhones(Human human){
        if(phoneBook.containsKey(human)){
            return phoneBook.get(human);
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneBook phoneBook1 = (PhoneBook) o;
        return Objects.equals(phoneBook, phoneBook1.phoneBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneBook);
    }
}
