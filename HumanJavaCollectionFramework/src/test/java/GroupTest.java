import org.junit.Test;
import static org.junit.Assert.*;

public class GroupTest {
    @Test
    public void test(){
        Group group = new Group(100, 1, 2, 3);

        assertEquals(100, group.getId());
        assertArrayEquals(new int[]{1, 2, 3}, group.getArray());
        assertEquals(3, group.getLength());

        group.setId(10);
        group.setArray(11, 12, 13, 14, 15);

        assertEquals(10, group.getId());
        assertArrayEquals(new int[]{11, 12, 13, 14, 15}, group.getArray());
        assertEquals(5, group.getLength());
    }

}