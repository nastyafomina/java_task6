import org.junit.Test;
import static org.junit.Assert.*;
public class HumanTest {
    @Test
    public void test(){
        Human human = new Human("Ivanov", "Ivan", "Ivanovich", 42);

        assertEquals("Ivanov", human.getSurname());
        assertEquals("Ivan", human.getName());
        assertEquals("Ivanovich", human.getPatronymic());
        assertEquals(42, human.getAge());

        human.setSurname("Petrov");
        human.setName("Petr");
        human.setPatronymic("Petrovich");
        human.setAge(31);

        assertEquals("Surname: Petrov\nName: Petr\nPatronymic: Petrovich\nAge: 31 years old\n", human.toString());
    }
}
