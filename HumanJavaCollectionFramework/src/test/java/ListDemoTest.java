import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class ListDemoTest {
    @Test
    public void test(){
        ListDemo listDemo = new ListDemo();

        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 55);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 17);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 15);
        Human human4 = new Human("Petrov", "Dmitriy", "Dmitrievich", 55);
        Human person = new Human("Petrov", "Aleksandr", "Aleksandrovich", 34);

        List<Human> list = new ArrayList<>();
        list.add(human1);
        list.add(human2);
        list.add(human3);
        list.add(human4);
        List<Human> listHomonyms = new ArrayList<>();
        listHomonyms.add(human2);
        listHomonyms.add(human4);

        assertEquals(listHomonyms, listDemo.getListOfHomonyms(list, person));

        List<Human> listOldest = new ArrayList<>();
        listOldest.add(human1);
        listOldest.add(human4);

        assertEquals(listOldest, listDemo.getTheOldestHumans(list));
    }

    @Test
    public void test2() {
        ListDemo listDemo = new ListDemo();
        // ну пустой, так пустой. Хотя можно выкидывать исключение, но пусть будет так
        assertEquals(new ArrayList<Human>(), listDemo.getTheOldestHumans(new ArrayList<>()));
    }
}