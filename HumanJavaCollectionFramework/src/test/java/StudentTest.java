import org.junit.Test;
import static org.junit.Assert.*;

public class StudentTest {
    @Test
    public void test(){
        Student student = new Student("Petrov", "Petr", "Petrovich", 19, "IMIT");

        assertEquals("IMIT", student.getFaculty());

        student.setFaculty("PhysFac");
        assertEquals("Surname: Petrov\nName: Petr\nPatronymic: Petrovich\nAge: 19 years old\nFaculty: PhysFac\n", student.toString());
    }

}