import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class PhoneBookTest {
    @Test
    public void test(){
        PhoneBook phoneBook = new PhoneBook();

        Human human1 = new Human("Vasilev", "Vasiliy", "Vasilevich", 34);
        Human human2 = new Human("Ivanov", "Ivan", "Ivanovich", 42);
        Human human3 = new Human("Ivanenko", "Petr", "Petrovich", 67);
        Human human4 = new Human("Stepanov", "Stepan", "Stepanovich", 25);

        List<String> list1 = new ArrayList<>();
        list1.add("112233");
        List<String> list2 = new ArrayList<>();
        list2.add("223344");
        List<String> list3 = new ArrayList<>();
        list3.add("334455");
        list3.add("334466");
        List<String> list4 = new ArrayList<>();
        list4.add("445566");

        phoneBook.addHuman(human1, list1);
        phoneBook.addHuman(human2, list2);
        phoneBook.addHuman(human3, list3);
        phoneBook.addHuman(human4, list4);

        //--------------------------------------------------------------------------------------------------------------

        Human humanExpected1 = new Human(human1.getSurname(), human1.getName(), human1.getPatronymic(), human1.getAge());
        Human humanExpected2 = new Human(human2.getSurname(), human2.getName(), human2.getPatronymic(), human2.getAge());
        Human humanExpected3 = new Human(human3.getSurname(), human3.getName(), human3.getPatronymic(), human3.getAge());
        Human humanExpected4 = new Human(human4.getSurname(), human4.getName(), human4.getPatronymic(), human4.getAge());

        List<String> listExpected1 = new ArrayList<>();
        listExpected1.add("112233");
        List<String> listExpected2 = new ArrayList<>();
        listExpected2.add("223344");
        List<String> listExpected3 = new ArrayList<>();
        listExpected3.add("334455");
        listExpected3.add("334466");
        List<String> listExpected4 = new ArrayList<>();
        listExpected4.add("445566");

        //--------------------------------------------------------------------------------------------------------------

        assertEquals(null, phoneBook.getPhones(new Human()));

        //--------------------------------------------------------------------------------------------------------------

        phoneBook.addPhone(human2, "778899");
        listExpected2.add("778899");
        assertEquals(listExpected2, phoneBook.getPhones(human2));

        //--------------------------------------------------------------------------------------------------------------

        phoneBook.deletePhone(human3, "334455");
        listExpected3.remove("334455");
        assertEquals(listExpected3, phoneBook.getPhones(human3));

        //--------------------------------------------------------------------------------------------------------------

        assertEquals(human4, phoneBook.getHumanByPhone("445566"));
        assertEquals(null, phoneBook.getHumanByPhone("990099"));

        //--------------------------------------------------------------------------------------------------------------

        PhoneBook phoneBookByBeginningOfSurname = new PhoneBook();

        phoneBookByBeginningOfSurname.addHuman(human2, list2);
        phoneBookByBeginningOfSurname.addHuman(human3, list3);

        PhoneBook phoneBook1 = phoneBook.getHumansWithPhonesByBeginningOfSurname("Iva");
        assertEquals(phoneBookByBeginningOfSurname, phoneBook1);
        assertEquals(new PhoneBook(), phoneBook.getHumansWithPhonesByBeginningOfSurname("Mak"));
    }

    @Test
    public void test2() {
        PhoneBook phoneBook = new PhoneBook();

        Human human1 = new Human("Vasilev", "Vasiliy", "Vasilevich", 34);

        phoneBook.addPhone(human1, "11111");
    }

    @Test
    public void test3() {
        PhoneBook phoneBook = new PhoneBook();

        Human human1 = new Human("Vasilev", "Vasiliy", "Vasilevich", 34);

        phoneBook.deletePhone(human1, "11111");
    }
}