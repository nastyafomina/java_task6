import org.junit.Test;

import static org.junit.Assert.*;

public class DataTest {
    @Test
    public void test() {
        Group group1 = new Group(1, 1, 2, 3);
        Group group2 = new Group(2, 4, 5, 6, 7);
        Group group3 = new Group(3, 8, 9);
        Group group4 = new Group(4, 10);

        Data data = new Data("data", group1, group2, group3, group4);
        Group[] groups = {group1, group2, group3, group4};

        assertEquals("data", data.getName());
        assertArrayEquals(groups, data.getGroups());
        assertEquals(4, data.getLength());
        assertEquals(10, data.getCount());

        Group group5 = new Group(5, 1,3);
        Group group6 = new Group(6, 2,4,6);

        data.setName("Data");
        data.setGroups(group5, group6);
        Group[] groupsNew = {group5, group6};

        assertEquals("Data", data.getName());
        assertArrayEquals(groupsNew, data.getGroups());
        assertEquals(2, data.getLength());
        assertEquals(5, data.getCount());
    }
}