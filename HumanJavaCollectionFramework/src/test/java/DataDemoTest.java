import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class DataDemoTest {
    @Test
    public  void test(){
        DataDemo dataDemo = new DataDemo();
        Data data = new Data("data", new Group(1, 1, 2, 3), new Group(2, 4, 5, 6, 7),
                new Group(3, 8, 9), new Group(4, 10));
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);

        assertEquals(list, dataDemo.getAll(data));
    }
}