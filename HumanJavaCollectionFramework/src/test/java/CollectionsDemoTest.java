import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class CollectionsDemoTest {
    @Test
    public void testCheckFistElement(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        ArrayList<String> list = new ArrayList<>();
        list.add("cat");
        list.add("dog");
        list.add("carrot");
        list.add("rabbit");
        list.add("ring");
        list.add("rainbow");

        assertEquals(1, collectionsDemo.checkFistElement(list, 'd'));
        assertEquals(2, collectionsDemo.checkFistElement(list, 'c'));
        assertEquals(3, collectionsDemo.checkFistElement(list, 'r'));
    }
    
    @Test
    public void testCopyListWithoutPerson(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 23);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 31);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 42);
        Human human4 = new Human("Dmitriev", "Dmitriy", "Dmitrievich", 55);

        ArrayList<Human> list = new ArrayList<>();
        list.add(human1);
        list.add(human2);
        list.add(human3);
        list.add(human4);

        ArrayList<Human> listNew = new ArrayList<>();
        listNew.add(human1);
        listNew.add(human3);
        listNew.add(human4);

        assertEquals(listNew, (ArrayList<Human>) collectionsDemo.copyListWithoutPerson(list, human2));
    }

    @Test
    public void testDisjointSets(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);

        Set<Integer> set2 = new HashSet<>();
        set2.add(3);
        set2.add(4);

        Set<Integer> set3 = new HashSet<>();
        set3.add(5);
        set3.add(6);

        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(4);

        ArrayList<Set<Integer>> list = new ArrayList<>();
        list.add(set1);
        list.add(set2);
        list.add(set3);

        ArrayList<Set<Integer>> listNew = new ArrayList<>();
        listNew.add(set1);
        listNew.add(set2);

        assertEquals(listNew, collectionsDemo.disjointSets(list, set));
    }

    @Test
    public void testDisjointSets2(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Set<Integer> set1 = new HashSet<>();
        set1.add(1);

        Set<Integer> set2 = new HashSet<>();
        set2.add(1);

        Set<Integer> set3 = new HashSet<>();
        set3.add(1);

        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);

        ArrayList<Set<Integer>> list = new ArrayList<>();
        list.add(set1);
        list.add(set2);
        list.add(set3);

        ArrayList<Set<Integer>> listNew = new ArrayList<>();
        // сюда нужно добавить все множества, потому что они все пересекаются с set\
        listNew.add(set1);
        listNew.add(set2);
        listNew.add(set3);

        assertEquals(listNew, collectionsDemo.disjointSets(list, set));
    }

    @Test
    public void testGetListAccordingToIdentifiers(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Map<Integer, Human> map = new HashMap<>();
        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 23);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 31);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 42);
        Human human4 = new Human("Dmitriev", "Dmitriy", "Dmitrievich", 55);
        map.put(11, human1);
        map.put(12, human2);
        map.put(13, human3);
        map.put(14, human4);

        Set<Human> setHuman = new HashSet<>();
        setHuman.add(human2);
        setHuman.add(human3);

        Set<Integer> setNumbers = new HashSet<>();
        setNumbers.add(12);
        setNumbers.add(13);

        assertEquals(setHuman, collectionsDemo.getSetAccordingToIdentifiers(map, setNumbers));
    }

    @Test
    public void testAdultsIdList(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Map<Integer, Human> map = new HashMap<>();
        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 23);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 17);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 15);
        Human human4 = new Human("Dmitriev", "Dmitriy", "Dmitrievich", 55);
        map.put(11, human1);
        map.put(12, human2);
        map.put(13, human3);
        map.put(14, human4);

        List<Integer> listId = new ArrayList<>();
        listId.add(11);
        listId.add(14);

        assertEquals(listId, collectionsDemo.adultsIdList(map));
    }

    @Test
    public void testMapIdAge(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Map<Integer, Human> map = new HashMap<>();
        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 23);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 17);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 15);
        Human human4 = new Human("Dmitriev", "Dmitriy", "Dmitrievich", 55);
        map.put(11, human1);
        map.put(12, human2);
        map.put(13, human3);
        map.put(14, human4);

        Map<Integer, Integer> mapAge = new HashMap<>();
        mapAge.put(11, 23);
        mapAge.put(12, 17);
        mapAge.put(13, 15);
        mapAge.put(14, 55);

        assertEquals(mapAge, collectionsDemo.mapIdAge(map));
    }

    @Test
    public void testCoevalsMap(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Map<Integer, Human> map = new HashMap<>();
        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 17);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 17);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 15);
        Human human4 = new Human("Dmitriev", "Dmitriy", "Dmitrievich", 55);
        map.put(11, human1);
        map.put(12, human2);
        map.put(13, human3);
        map.put(14, human4);

        Set<Human> set = new HashSet<>();
        set.add(human1);
        set.add(human2);
        set.add(human3);
        set.add(human4);

        Map<Integer, List<Human>> mapAge = new HashMap<>();
        List<Human> list1 = new ArrayList<>();
        List<Human> list2 = new ArrayList<>();
        List<Human> list3 = new ArrayList<>();
        list1.add(human1);
        list1.add(human2);
        list2.add(human3);
        list3.add(human4);
        mapAge.put(human1.getAge(), list1);
        mapAge.put(human3.getAge(), list2);
        mapAge.put(human4.getAge(), list3);

        assertEquals(mapAge, collectionsDemo.coevalsMap(set));
    }

    @Test
    public void testMapAgeAlphabetSurname(){
        CollectionsDemo collectionsDemo = new CollectionsDemo();

        Map<Integer, Map<Character, List<Human>>> map = new HashMap<>();

        Human human1 = new Human("Ivanov", "Ivan", "Ivanovich", 17);
        Human human2 = new Human("Petrov", "Petr", "Petrovich", 17);
        Human human3 = new Human("Stepanov", "Stepan", "Stepanovich", 15);
        Human human4 = new Human("Dmitriev", "Dmitriy", "Dmitrievich", 55);
        Human human5 = new Human("Pavlov", "Pavel", "Pavlovich", 17);

        Set<Human> set = new HashSet<>();
        set.add(human1);
        set.add(human2);
        set.add(human3);
        set.add(human4);
        set.add(human5);

        List<Human> list1 = new ArrayList<>();
        List<Human> list2 = new ArrayList<>();
        List<Human> list3 = new ArrayList<>();
        List<Human> list4 = new ArrayList<>();
        list1.add(human4);
        list2.add(human1);
        list3.add(human2);
        list3.add(human5);
        list4.add(human3);
        Comparator comparator = new Comparator<Human>() {
            @Override
            public int compare(Human o1, Human o2) {
                if(o1.getSurname().compareTo(o2.getSurname()) == 0 && o1.getName().compareTo(o2.getName()) == 0) {
                    return o1.getName().compareTo(o2.getName()); }
                if(o1.getSurname().compareTo(o2.getName()) == 0) {
                    return o1.getName().compareTo(o2.getName());
                }
                return o1.getSurname().compareTo(o2.getSurname());
            }
        };
        Collections.sort(list1, comparator);
        Collections.sort(list2, comparator);
        Collections.sort(list3, comparator);
        Collections.sort(list4, comparator);

        Map<Character, List<Human>> mapChar1 = new HashMap<>();
        Map<Character, List<Human>> mapChar2 = new HashMap<>();
        Map<Character, List<Human>> mapChar3 = new HashMap<>();
        mapChar1.put('D', list1);
        mapChar2.put('I', list2);
        mapChar2.put('P', list3);
        mapChar3.put('S', list4);

        map.put(human4.getAge(), mapChar1);
        map.put(human1.getAge(), mapChar2);
        map.put(human3.getAge(), mapChar3);

        assertEquals(map, collectionsDemo.mapAgeAlphabetSurname(set));
    }
}